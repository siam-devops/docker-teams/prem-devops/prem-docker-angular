
# Stage 1
FROM node:12-stretch
RUN mkdir -p /app
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
EXPOSE 4200
CMD ["npm", "start"]